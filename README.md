# MD Uebung files

This is a simple 2-dimensional MD program, but it is not complete. As homework, you have to write the missing parts in `md.c`.

This Uebung began with code from Marco Matthies and Nils Petersen.
In June 2022, I moved executable code out of `rvec.h`. As a matter of style, one should never have code or anything that allocates space in `.h` files.

I removed some unnecessary lines from the Makefile.

# BUGS
If you compile with `-Wall`, there will be some warnings about unused variables. These are not a problem. This is an assignment in which one should write code that uses these variables.
