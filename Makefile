.POSIX:

CFLAGS = -O

OBJS = md.o md-extra.o rvec.o
INCS = md.h rvec.h

md: $(OBJS) $(INCS)
	$(CC) $(CFLAGS) rvec.o md.o md-extra.o -o md -lm

clean:
	rm -f md a.out
	rm -f *.o
	rm -f *~
