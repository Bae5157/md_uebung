#ifndef RVEC_H
#define RVEC_H

#define NDIM 2
typedef double real;
typedef real rvec[NDIM];
typedef unsigned int uint;
typedef unsigned long ulong;

void rvec_set(rvec *u, real val);
void rvecary_set(uint n, rvec *a, real val);
void rvec_print(rvec u);
void rvecary_print(rvec *a, uint n);
void rvec_mul(rvec *u, real a, rvec v);
void rvec_add(rvec *u, rvec v, rvec w);
void rvecary_muladd(uint n, rvec *a, rvec *b, real r, rvec *c);
void rvec_muladd(rvec *u, rvec v, real r, rvec w);
real rvec_len(rvec u);
void rvec_rand(rvec *u);
void rvec_sub_pbc(rvec *u, rvec box, rvec v, rvec w);
real rvec_dot(rvec u, rvec v);
#endif /* RVEC_H */
 
